package untitled.chemicraft;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import untitled.chemicraft.blocks.BlockGalium;
import untitled.chemicraft.blocks.Spectrometer.SpectoMeter;
import untitled.chemicraft.blocks.Spectrometer.SpectoMeterTileEntity;
import untitled.chemicraft.blocks.liquids.Liquidgalium;
import untitled.chemicraft.blocks.liquids.Liquidmercury;
import untitled.chemicraft.client.GuiHandler;
import untitled.chemicraft.items.Vial;
import untitled.chemicraft.util.ElementRegistry;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.event.FMLServerStoppingEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;

@Mod(modid = Reference.MOD_ID, name = Reference.MOD_NAME)
public class ChemiCraft {
	
	public static CreativeTabs tabChemiCraft = new CreativeTabs("tabChemicraft") {
		@Override
		public Item getTabIconItem() {
			return vial;
		}
	};
	
	public final static Block SpectoMeterBlock = new SpectoMeter(
			Material.ground).setHardness(0.5F)
			.setStepSound(Block.soundTypeAnvil)
			.setBlockName("Mass_SpectoMeter")
			.setCreativeTab(tabChemiCraft);
	public static Item vial;
	//Liquid Mercury
	public static Fluid liquidmercury2 = new Fluid("liquidmercury").setTemperature(500);
	public static Block liquidmercury;
	//Liquid Galium
	public static Block liquidgalium;
	public static Fluid liquidgalium2 = new Fluid("liquidgalium");
	//Solid Galium
	public static Block galium = new BlockGalium(Material.iron).setHardness(0.5F)
			.setStepSound(Block.soundTypeAnvil)
			.setBlockName("Mass_SpectoMeter")
			.setCreativeTab(tabChemiCraft);
	

	@Instance
	public static ChemiCraft instance;

	 @SidedProxy(clientSide = "untitled.chemicraft.client.ClientProxy", serverSide = "untitled.chemicraft.CommonProxy")
	 public static CommonProxy proxy;

	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		//Register Mercury
		FluidRegistry.registerFluid(liquidmercury2);
		liquidmercury = new Liquidmercury(liquidmercury2, Material.water);
		liquidmercury.setCreativeTab(tabChemiCraft);
		liquidmercury2.setUnlocalizedName(liquidmercury.getUnlocalizedName());
		GameRegistry.registerBlock(liquidmercury, "Liquid_mercury");
		//Register galium - Liquid form
		FluidRegistry.registerFluid(liquidgalium2);
		liquidgalium = new Liquidgalium(liquidgalium2, Material.water);
		GameRegistry.registerBlock(liquidgalium, "Liquid_galium");
		//Register Galium - Block form
		GameRegistry.registerBlock(galium, "galium");
		//Register Mass Spectometer
		GameRegistry.registerBlock(SpectoMeterBlock, "Mass_SpectoMeter");
		GameRegistry.registerTileEntity(SpectoMeterTileEntity.class,
				"SpectoMeterTileEntity");
		// Register Vial
		vial = new Vial();
		GameRegistry.registerItem(vial, "Vial_Empty");
	}

	@EventHandler
	public void init(FMLInitializationEvent event) {
		NetworkRegistry.INSTANCE.registerGuiHandler(this, new GuiHandler());
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		//Start Element Registry up
		ElementRegistry.Init();
		
	}

	@EventHandler
	public void serverstart(FMLServerStartingEvent event) {

	}

	@EventHandler
	public void serverstop(FMLServerStoppingEvent event) {

	}



}
