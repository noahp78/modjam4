package untitled.chemicraft.blocks;

import untitled.chemicraft.ChemiCraft;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class BlockChemiCraftMain extends Block {

	protected BlockChemiCraftMain(Material material) {
		super(material);
		setCreativeTab(ChemiCraft.tabChemiCraft);
	}
	
	protected BlockChemiCraftMain() {
		this(Material.iron);
	}

}
