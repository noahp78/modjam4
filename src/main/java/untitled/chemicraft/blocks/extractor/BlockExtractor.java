package untitled.chemicraft.blocks.extractor;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import untitled.chemicraft.blocks.BlockChemiCraftMain;
import untitled.chemicraft.blocks.BlockChemiCraftTile;

public class BlockExtractor extends BlockChemiCraftTile {

	@Override
	public TileEntity createNewTileEntity(World var1, int var2) {
		return new TileExtractor();
	}
	

}
