package untitled.chemicraft.blocks;

import untitled.chemicraft.ChemiCraft;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public abstract class BlockChemiCraftTile extends BlockContainer {
	
	protected BlockChemiCraftTile(Material material) {
		super(material);
		setCreativeTab(ChemiCraft.tabChemiCraft);
	}
	
	protected BlockChemiCraftTile() {
		this(Material.iron);
	}

}
