package untitled.chemicraft.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import untitled.chemicraft.ChemiCraft;
import untitled.chemicraft.blocks.liquids.Liquidgalium;

public class BlockGalium extends Block{

	public BlockGalium(Material m) {
		super(m);
		setCreativeTab(ChemiCraft.tabChemiCraft);
		setBlockTextureName("chemicraft:galium");
		
		// TODO Auto-generated constructor stub
	}
	@Override
	public void onEntityWalking(World world, int x, int y, int z, Entity en) {
		if(en instanceof EntityPlayer){
			world.setBlock(x, y, z, ChemiCraft.liquidgalium);
		}
		
	}

}
