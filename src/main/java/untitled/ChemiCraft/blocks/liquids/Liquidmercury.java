package untitled.chemicraft.blocks.liquids;

import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fluids.BlockFluidClassic;
import net.minecraftforge.fluids.Fluid;
import untitled.chemicraft.ChemiCraft;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class Liquidmercury extends BlockFluidClassic {
		

        public Liquidmercury(Fluid fluid, Material material) {
		super(fluid, material);
        setCreativeTab(ChemiCraft.tabChemiCraft);
		// TODO Auto-generated constructor stub
	}
		@SideOnly(Side.CLIENT)
        protected IIcon stillIcon;
        @SideOnly(Side.CLIENT)
        protected IIcon flowingIcon;
        public IIcon getIcon(int side, int meta) {
            return (IIcon) ((side == 0 || side == 1)? stillIcon : flowingIcon);
    }
    
    
    @Override
    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister register){
            stillIcon =  register.registerIcon("chemicraft:mercuryStill");
            flowingIcon = register.registerIcon("chemicraft:mercuryFlowing");
    }
    
    @Override
    public boolean canDisplace(IBlockAccess world, int x, int y, int z) {
            if (world.getBlock(x,  y,  z).getMaterial().isLiquid()) return false;
            return super.canDisplace(world, x, y, z);
    }
    
    @Override
    public boolean displaceIfPossible(World world, int x, int y, int z) {
            if (world.getBlock(x,  y,  z).getMaterial().isLiquid()) return false;
            return super.displaceIfPossible(world, x, y, z);
    }
    @Override
	public void onEntityCollidedWithBlock(World world, int x, int y, int z, Entity entity){
    	if(entity instanceof EntityLivingBase){
    		((EntityLivingBase) entity).addPotionEffect(new PotionEffect(Potion.poison.id, 12 * 20, 0));
    	}
    }
    
}
