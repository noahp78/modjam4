package untitled.chemicraft;

import java.io.InputStream;
import java.util.Properties;

import com.google.common.base.Throwables;

public class Reference {
	public static final String MOD_ID = "ChemiCraft";

	public static final String MOD_NAME = "ChemiCraft";

	public static final String MOD_VERSION = getProp("version");
	
	private static String getProp(String id) {
		if (!isPropSetup) {
			try {
				InputStream stream = Reference.class
						.getResourceAsStream("reference.properties");
				prop.load(stream);
				stream.close();
			} catch (Exception e) {
				Throwables.propagate(e);
			}
			isPropSetup = true;
		}
		return prop.getProperty(id);
	}

	private static boolean isPropSetup = false;

	private static final Properties prop = new Properties();
}
