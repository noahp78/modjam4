package untitled.chemicraft.util;

import java.util.HashMap;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.util.StatCollector;

public class ElementRegistry {
	private  static HashMap<Block, String>ElementBlocks=new HashMap<Block,String>();
	
	public static String Get(Block b){
		if(ElementBlocks.get(b)!=null){
			return ElementBlocks.get(b);
		}else{
			return "Unknown";
		}
	}
	public static void Add(Block b, String s){
		ElementBlocks.put(b, s);
	}
	public static void Init(){
		ElementBlocks.put(Blocks.iron_block, StatCollector.translateToLocal(ge("Fe")));
		ElementBlocks.put(Blocks.gold_block, StatCollector.translateToLocal(ge("AU")));
		ElementBlocks.put(Blocks.diamond_block, StatCollector.translateToLocal(ge("C")));
		
		
		
		
	}
	public static String ge(String E){
		return ("chemicraft:element_" + E);
		
	}
}
