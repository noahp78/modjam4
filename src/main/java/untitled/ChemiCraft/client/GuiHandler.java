package untitled.chemicraft.client;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import untitled.chemicraft.blocks.Spectrometer.SpectoMeterContainer;
import untitled.chemicraft.blocks.Spectrometer.SpectoMeterTileEntity;
import untitled.chemicraft.blocks.Spectrometer.SpectometerGUI;
import cpw.mods.fml.common.network.IGuiHandler;

public class GuiHandler implements IGuiHandler {
	// returns an instance of the Container you made earlier
	@Override
	public Object getServerGuiElement(int id, EntityPlayer player, World world,
			int x, int y, int z) {
		TileEntity tileEntity = world.getTileEntity(x, y, z);
		if (tileEntity instanceof SpectoMeterTileEntity) {
			return new SpectoMeterContainer(player.inventory,
					(SpectoMeterTileEntity) tileEntity);
		}
		return null;
	}

	// returns an instance of the Gui you made earlier
	@Override
	public Object getClientGuiElement(int id, EntityPlayer player, World world,
			int x, int y, int z) {
		TileEntity tileEntity = world.getTileEntity(x, y, z);
		if (tileEntity instanceof SpectoMeterTileEntity) {
			return new SpectometerGUI(player.inventory,
					(SpectoMeterTileEntity) tileEntity);
		}
		return null;

	}
}