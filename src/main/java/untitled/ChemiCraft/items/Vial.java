package untitled.chemicraft.items;

import java.awt.List;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import untitled.chemicraft.ChemiCraft;
import untitled.chemicraft.util.ElementRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class Vial extends Item {
	public Vial() {
		maxStackSize = 16;
		setCreativeTab(ChemiCraft.tabChemiCraft);
		setUnlocalizedName("Vial_Empty");
		setTextureName("ChemiCraft:vial");
		setHasSubtypes(true);
        setMaxDamage(0); 
        
        
	}
	public static IIcon filled;
	public static IIcon empty;
	@Override
	public void registerIcons(IIconRegister icon) {
		filled = icon.registerIcon("chemicraft:vial");
		empty = icon.registerIcon("chemicraft:vial");

	}
	//Get Costum Icon, for now empty
    @Override
    public IIcon getIconFromDamage(int i){
            if(i!=0){
            	return empty;
            }else{
            	return filled;
            }
    }
    public String getUnlocalizedName(ItemStack stack)
    {
    	if(stack.getItemDamage()!=0){
        return "item." + "vial." + Block.getBlockById(stack.getItemDamage());
    	}else{
    		return ("item.vial.empty");
    	}
    }
    @Override
    public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10){
    	if (!par3World.isRemote) {
    	
    		
    	Block current = par3World.getBlock(par4, par5, par6);
		int ID = Block.getIdFromBlock(par3World.getBlock(par4, par5, par6));
    	String name = Block.getBlockById(ID).getLocalizedName();
    	ItemStack stack = par1ItemStack;
    	stack.setItemDamage(ID);
    	stack.setStackDisplayName("Vial of: " + ElementRegistry.Get(current));
    	EntityPlayer player = (EntityPlayer)par2EntityPlayer;

    	  player.inventory.setInventorySlotContents(player.inventory.currentItem, stack);
    	par2EntityPlayer.setCurrentItemOrArmor(0, null);
    	par2EntityPlayer.setCurrentItemOrArmor(0, stack);
		return true;
    	}else{
    		return true;
    	}
    			
    }
}
